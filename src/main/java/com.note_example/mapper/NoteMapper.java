package com.note_example.mapper;

import com.note_example.model.Note;
import com.note_example.model.NoteDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface NoteMapper {
    Note insert(Note note);

    List<Note> getAll();

    List<NoteDTO> getAllWithTasks();

    Note getById(Long id);

    void delete(Note note);

    Note update(Note note);

    List<Note> getByTaskId(Long taskId);

    List<Note> getAllByTaskIdIn(List<Long> taskIds);
}
