package com.note_example.mapper;

import com.note_example.model.Task;
import com.note_example.model.TaskDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TaskMapper {
    void insert(Task task);

    List<Task> getAll();

    List<TaskDTO> getAllWithNotes();

    Task getById(Long id);

    void delete(Task task);

    void update(Task task);
}
