package com.note_example.repository;

import com.note_example.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

    List<Note> getByTaskId(Long taskId);
    List<Note> getAllByTaskIdIn(List<Long> taskIds);
}