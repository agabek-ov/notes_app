package com.note_example.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NoteDTO{

    private Long id;

    private String title;

    private String content;

    private Long taskId;

    private Date createdAt;

    private Date updatedAt;

    private Task task;
}