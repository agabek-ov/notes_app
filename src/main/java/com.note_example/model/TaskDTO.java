package com.note_example.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TaskDTO {

    private Long id;

    private String title;

    private Boolean done;

    private Date doneAt;

    private List<Note> notes;

    public TaskDTO(Task task){
        this.id = task.getId();
        this.title = task.getTitle();
        this.done = task.getDone();
        this.doneAt = task.getDoneAt();
        this.notes = new ArrayList<Note>();
    }

    public void addNote(Note note){ notes.add(note);}

    public void addNotes(List<Note> notes) {
        this.notes.addAll(notes);
    }

    public static List<TaskDTO> getTaskDTOs (List<Task> tasks) {
        List<TaskDTO> taskDTOs = new ArrayList<TaskDTO>();

        for (Task task: tasks)
            taskDTOs.add(new TaskDTO(task));

        return taskDTOs;
    }
}
