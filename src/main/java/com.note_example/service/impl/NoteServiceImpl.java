
package com.note_example.service.impl;

import com.note_example.dao.NoteDAO;
import com.note_example.model.Note;
import com.note_example.model.NoteDTO;
import com.note_example.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteDAO noteDAO;

    @Override
    public Note insert(Note note) {
        return noteDAO.insert(note);
    }

    @Override
    public List<Note> getAll() {
        return noteDAO.getAll();
    }

    @Override
    public List<NoteDTO> getAllWithTasks() {
        return noteDAO.getAllWithTasks();
    }

    @Override
    public Note getById(Long id) {
        return noteDAO.getById(id);
    }

    @Override
    public void delete(Note note) {
        noteDAO.delete(note);
    }

    @Override
    public Note update(Note note) {
        return noteDAO.update(note);
    }

    @Override
    public List<Note> getByTaskId(Long taskId) {
        return noteDAO.getByTaskId(taskId);
    }

    @Override
    public List<Note> getAllByTaskIdIn(List<Long> taskIds) {
        return noteDAO.getAllByTaskIdIn(taskIds);
    }
}
