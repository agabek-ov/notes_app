
package com.note_example.service.impl;

import com.note_example.dao.NoteDAO;
import com.note_example.dao.TaskDAO;
import com.note_example.model.Note;
import com.note_example.model.Task;
import com.note_example.model.TaskDTO;
import com.note_example.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Override
    public Task insert(Task task) {
        return taskDAO.insert(task);
    }

    @Override
    public List<Task> getAll() {
        return taskDAO.getAll();
    }

    @Override
    public List<TaskDTO> getAllWithNotes() {
        return taskDAO.getAllWithNotes();
    }

    @Override
    public Task getById(Long id) {
        return taskDAO.getById(id);
    }

    @Override
    public void delete(Task task) {
        taskDAO.delete(task);
    }

    @Override
    public Task update(Task task) {
        return taskDAO.update(task);
    }
}
