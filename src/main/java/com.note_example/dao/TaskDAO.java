package com.note_example.dao;

import com.note_example.model.Task;
import com.note_example.model.TaskDTO;

import java.util.List;

public interface TaskDAO {
    Task insert(Task task);

    List<Task> getAll();

    List<TaskDTO> getAllWithNotes();

    Task getById(Long id);

    void delete(Task task);

    Task update(Task task);
}
