
package com.note_example.dao.impl;

import com.note_example.dao.NoteDAO;
import com.note_example.model.Note;
import com.note_example.mapper.NoteMapper;
import com.note_example.model.NoteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NoteDAOImpl implements NoteDAO {

    @Autowired
    private NoteMapper noteMapper;

    @Override
    public Note insert(Note note) {
        return noteMapper.insert(note);
    }

    @Override
    public List<Note> getAll() {
        return noteMapper.getAll();
    }

    @Override
    public List<NoteDTO> getAllWithTasks() {
        return noteMapper.getAllWithTasks();
    }

    @Override
    public Note getById(Long id) {
        return noteMapper.getById(id);
    }

    @Override
    public void delete(Note note) {
        noteMapper.delete(note);
    }

    @Override
    public Note update(Note note) {
        return noteMapper.update(note);
    }

    @Override
    public List<Note> getByTaskId(Long taskId) {
        return noteMapper.getByTaskId(taskId);
    }

    @Override
    public List<Note> getAllByTaskIdIn(List<Long> taskIds) {
        return noteMapper.getAllByTaskIdIn(taskIds);
    }
}
