package com.note_example.dao.impl;

import com.note_example.dao.TaskDAO;
import com.note_example.mapper.TaskMapper;
import com.note_example.model.Task;
import com.note_example.model.TaskDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    private TaskMapper taskMapper;

    @Override
    public Task insert(Task task){
        taskMapper.insert(task);
        return taskMapper.getById(task.getId());
    }

    @Override
    public List<Task> getAll() {
        return taskMapper.getAll();
    }

    @Override
    public List<TaskDTO> getAllWithNotes() {
        return taskMapper.getAllWithNotes();
    }

    @Override
    public Task getById(Long id) {
        return taskMapper.getById(id);
    }

    @Override
    public void delete(Task task) {
        taskMapper.delete(task);
    }

    @Override
    public Task update(Task task){
        taskMapper.update(task);
        return taskMapper.getById(task.getId());
    }
}
