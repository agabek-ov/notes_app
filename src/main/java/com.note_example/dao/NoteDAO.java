package com.note_example.dao;

import com.note_example.model.Note;
import com.note_example.model.NoteDTO;

import java.util.List;

public interface NoteDAO {
    Note insert(Note note);

    List<Note> getAll();

    List<NoteDTO> getAllWithTasks();

    Note getById(Long id);

    void delete(Note note);

    Note update(Note note);

    List<Note> getByTaskId(Long taskId);

    List<Note> getAllByTaskIdIn(List<Long> taskIds);
}
