package com.note_example.controller;

import com.note_example.exception.ResourceNotFoundException;

import com.note_example.model.Note;
import com.note_example.model.NoteDTO;
import com.note_example.service.NoteService;
import com.note_example.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NoteController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private NoteService noteService;

    // Get All Notes
    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        //return noteRepository.findAll();
        return noteService.getAll();
    }

    // Get All Notes With Tasks
    @GetMapping("/notesW")
    public List<NoteDTO> getAllNotesWithTasks() {
        //return noteRepository.findAll();
        return noteService.getAllWithTasks();
    }

    // Create a new Note
    @PostMapping("/notes")
    public Note createNote(@Valid @RequestBody Note note) {
        try {
            taskService.getById(note.getTaskId());
        } catch (Exception e) {
            throw new ResourceNotFoundException("Task", "id", note.getTaskId());
        }
        return noteService.insert(note);
    }

    // Get a Single Note
    @GetMapping("/notes/{id}")
    public Note getNoteById(@PathVariable(value = "id") Long noteId) {
        try {
            return noteService.getById(noteId);
        } catch (Exception e) {
            throw new ResourceNotFoundException("Note", "id", noteId);
        }
    }

    // Update a Note
    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable(value = "id") Long noteId,
                           @Valid @RequestBody Note noteDetails) {
        Note note;
        try {
            note = noteService.getById(noteId);
        } catch (Exception e) {
            throw new ResourceNotFoundException("Note", "id", noteId);
        }
        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());
        note.setTaskId(noteDetails.getTaskId());

        return noteService.update(note);
    }

    // Delete a Note
    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {

        Note note;
        try {
            note = noteService.getById(noteId);
        } catch (Exception e) {
            throw new ResourceNotFoundException("Note", "id", noteId);
        }

        noteService.delete(note);

        return ResponseEntity.ok().build();
    }
}