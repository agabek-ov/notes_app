package com.note_example.controller;

import com.note_example.exception.ResourceNotFoundException;
import com.note_example.model.Task;
import com.note_example.model.TaskDTO;

import com.note_example.service.NoteService;
import com.note_example.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskController {

    @Autowired
    private TaskService taskService;

    // Get All Notes
    @GetMapping("/tasks")
    public List<Task> getAllTasks() {
        return taskService.getAll();
    }

    @GetMapping("/tasksW")
    public List<TaskDTO> getAllTasksWithNotes() {
        return taskService.getAllWithNotes();
    }

    // Get a Single Note
    @GetMapping("/tasks/{id}")
    public Task getTaskById(@PathVariable(value = "id") Long taskId) {
        return taskService.getById(taskId);
    }

    // Create a new Note
    @PostMapping("/tasks")
    public Task createTask(@Valid @RequestBody Task task) {
        return taskService.insert(task);
    }

    // Update a Note
    @PutMapping("/tasks/{id}")
    public Task updateTask(@PathVariable(value = "id") Long taskId,
                           @Valid @RequestBody Task taskDetails) {

        Task task = taskService.getById(taskId);
        if(task == null) return null;
        task.setTitle(taskDetails.getTitle());
        task.setDone(taskDetails.getDone());
        if (task.getDone()) {
            Date in = new Date();
            LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
            Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            task.setDoneAt(out);
        }

        return taskService.update(task);
    }

    // Delete a Note
    @DeleteMapping("/tasks/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable(value = "id") Long taskId) {

        Task task = taskService.getById(taskId);
        if(task == null) return null;

        taskService.delete(task);

        return ResponseEntity.ok().build();
    }
}